export interface BookInformation {
    id?:number;
    title:string;
    author?:string;
    image?:string;
    isbn?:string;
    publishedDate?:string;
    review?:string;
    finished?:boolean;
    stars?:string;
    emoji?:string;
    bookShelf?:string;
}
