import { StatusBar } from 'expo-status-bar';
import { Button, StyleSheet, View } from 'react-native';
import { useNavigation } from '@react-navigation/native';



export default function HomeScreen():any {
    const navigation = useNavigation<any>();
    return (
        <View style={styles.container}>
        <Button title="Camera" onPress={() => navigation.navigate("Camera")} />
        <Button title="BookShelf" onPress={() => navigation.navigate("BookList")} />
        <StatusBar style="auto" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

