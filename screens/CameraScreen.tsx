import React, { useState, useEffect } from "react";
import { Text, StyleSheet, Button, SafeAreaView,Image,View} from "react-native";
import { BarCodeScanner } from "expo-barcode-scanner";
import axios from "axios";
import { useNavigation } from '@react-navigation/native';

export default function App() {
    const [hasPermission, setHasPermission] = useState<any | null>(null);
    const [scanned, setScanned] = useState(true);
    const [bookInfo,setBookInfo]=useState(null);
    const navigation = useNavigation<any>();

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === "granted");
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }:any) => {
        setScanned(true);
        const URL = 'https://www.googleapis.com/books/v1/volumes?q=isbn:'+data;
        alert(`Bar code with type ${type} and data ${data} has been scanned!${URL}`);
        fetchBooks(URL);
        console.log(URL);
    };

    const fetchBooks = async (URL:any) => {
        try {
        const response = await axios.get(URL);
          //mettre un information d'un livre
        setBookInfo(response.data.items[0]);
        } catch (error) {
        alert("error occurred");
        }
    }; 

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    const styles = StyleSheet.create({
        barcode: {
            width: "100%",
            height: "40%"
        },
        container: {
            flex: 1
        }
    });
    const resetSession = () => {
        setScanned(false);
        setBookInfo(null);
    };
    return (
        <SafeAreaView style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={styles.barcode}
            ></BarCodeScanner>
            {scanned && (
                <Button title={"Tap to Scan Again"} onPress={() => resetSession()}  />
            )}
            {bookInfo !==null?(
                <>
                <Text>Le title de livre{bookInfo.volumeInfo.title} un jour de publication {bookInfo.volumeInfo.publishedDate}</Text>
                {bookInfo.volumeInfo.imageLinks?
                    (<Image source = {{uri:bookInfo.volumeInfo.imageLinks.thumbnail}} style={{width:100, height:150}}/>):
                    <View style={{width:100, height:150,backgroundColor:"lightgreen"}}><Text>No Image found</Text></View>
                }
                </>
            ):null}
            <Button title="registration" onPress={()=>{navigation.navigate("BookRegistration",{info:bookInfo})
        setBookInfo(null)}}></Button>
        </SafeAreaView>
    );
}