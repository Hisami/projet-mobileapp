import React, { useState, useEffect } from "react";
import { Button, StyleSheet, View, ScrollView, Text, Image, TextInput, Switch } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useForm, Controller } from "react-hook-form";
import { Picker } from '@react-native-picker/picker';
import { postBook } from "../bookshelfService";

export default function BookRegistration({ route }: any) {
    const navigation = useNavigation<any>();
    const { info } = route.params;
    const [bookInfo, setBookInfo] = useState(null);
    useEffect(() => {
        setBookInfo(info)
    }, [])
    const { control, handleSubmit, setValue, formState: { errors } } = useForm({
        defaultValues: {
            review: '',
            finished:false,
            stars: " "
        } 
    });
    const onSubmit = async(data: any) => {
        console.log(data)
        navigation.navigate("BookList")
        await postBook({ ...data, title: bookInfo.volumeInfo.title, author: bookInfo.volumeInfo.authors[0],isbn:bookInfo.volumeInfo.industryIdentifiers[1].identifier,image:bookInfo.volumeInfo.imageLinks.thumbnail,publishedData:bookInfo.volumeInfo.publishedDate});
    }
    return (
        <ScrollView style={styles.container}>
            {bookInfo &&
                <>
                    <Text>Le title de livre{bookInfo.volumeInfo.title} un jour de publication {bookInfo.volumeInfo.publishedDate}{bookInfo.volumeInfo.authors[0]}</Text>
                    {bookInfo.volumeInfo.imageLinks &&
                        <Image source={{ uri: bookInfo.volumeInfo.imageLinks.thumbnail }} style={{ width: 100, height: 100 }} />
                    }
                </>
            }
            <Text>Your opinion</Text>
            <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                        placeholder="Write Your opinion"
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                    />
                )}
                name="review"
            />
            {errors.review && <Text>This is required.</Text>}
            <Text>Read</Text>
            <Controller
                control={control}
                render={({ field: { onChange, value } }) => (
                    <Switch
                        onValueChange={onChange}
                        value={value}
                    />
                )}
                name="finished"
            />
            <Controller
                control={control}
                render={({ value }: any) => (
                    <View style={styles.picker}>
                        <Picker
                            selectedValue={value}
                            onValueChange={itemValue => setValue("stars", itemValue)}
                        >
                            <Picker.Item label="⭐️" value="1" />
                            <Picker.Item label="⭐️⭐️" value="2" />
                            <Picker.Item label="⭐️⭐️⭐️" value="3" />
                            <Picker.Item label="⭐️⭐️⭐️⭐️" value="4" />
                            <Picker.Item label="⭐️⭐️⭐️⭐️⭐️" value="5" />
                        </Picker>
                    </View>
                )}
                name="stars"
                defaultValue="3"
            />
            
            <Button title="Submit" onPress={handleSubmit(onSubmit)} />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container:{},
    picker: {
        backgroundColor: '#f2f2f2',
        marginBottom: 10,
        borderRadius: 6,
    },
})