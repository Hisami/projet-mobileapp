import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from "react";
import { Button, StyleSheet, View ,Text,Image} from 'react-native';
import axios from 'axios';
import { useNavigation } from '@react-navigation/native';

export default function BookShelf({navigation}:any) {
    const [bookInfo,setBookInfo]=useState(null);
    const fetchBooks = async () => {
        try {
        const response = await axios.get('https://www.googleapis.com/books/v1/volumes?q=isbn:9783140464079');
        console.log(response.data.items[0].volumeInfo.title);
        setBookInfo(response.data.items[0])
        } catch (error) {
        alert("error occurred");
        }
    }; 
    
    return (
        <View style={styles.container}>
        {bookInfo&&
        <>
        <Text>Le title de livre{bookInfo.volumeInfo.title} un jour de publication {bookInfo.volumeInfo.publishedDate}</Text>
        {bookInfo.volumeInfo.imageLinks.smallThumbnail&&
        <Image source = {{uri:bookInfo.volumeInfo.imageLinks.smallThumbnail}} style={{width:100, height:100}}/>
        }
        </>

        }
        <Button title="hello" onPress={()=>fetchBooks()}></Button>
        <Button title="registration" onPress={()=>navigation.navigate("BookRegistration",{info:bookInfo})}></Button>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
});