
import React, { useState, useEffect } from "react";
import {StyleSheet, View, Text, Image, FlatList,TouchableOpacity,Button,TextInput,Switch,ScrollView} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { BookInformation } from "../entities";
import { deleteBookInformation, fetchAllBooks, postBook, updateBookInformation } from "../bookshelfService";
import { useForm, Controller } from "react-hook-form";
import { Picker } from '@react-native-picker/picker';
import { BookCard } from "../components/BookCard";
import { BookInfo } from "../components/BookInfo";
import StarRating from "react-native-star-rating-widget";



export default function BookList({ route }: any) {
const [edit,setEdit]=useState<boolean>(false);
const [books,setBooks] =useState<BookInformation[]>();
const navigation = useNavigation<any>();
const { control, handleSubmit, setValue, formState: { errors } } = useForm({
    defaultValues: {
        review: '',
        finished:false,
        stars:0
    } 
});
    
    useEffect(() => {
        fetchAllBooks()
            .then(books => {setBooks(books["hydra:member"]);setBookInfo(books["hydra:member"][1])})
            .catch(err => console.log(err));
    }, []);
    console.log(books)
    const [bookInfo, setBookInfo] = useState<BookInformation>();
    const Item = (item : BookInformation) => (
        <TouchableOpacity onPress={()=>setBookInfo(item)} style={styles.item}>
            <BookCard title={item.title} image={item.image}  />
        </TouchableOpacity>
    );
    const onSubmit = async(data: any) => {
        console.log(data)
        await updateBookInformation({ ...data, id:bookInfo.id});
        setEdit(!edit)
    }
    const deleteBook = async()=>{
        await deleteBookInformation(bookInfo.id);
        setEdit(!edit);
        fetchAllBooks()
        .then(books => setBooks(books["hydra:member"]))
        .catch(err => console.log(err));
        setBookInfo(books[1])
    }

    return (
        <View style={styles.container}>
            {bookInfo&&
            <View >
            <BookInfo title={bookInfo.title} image={bookInfo.image} review={bookInfo.review}/>
            <Button title ={edit?"Back to BookShelf":"Edit"} onPress={()=>setEdit(!edit)}></Button>
            {edit&& (
                <ScrollView>
                    <Controller
                control={control}
                render={({ field: { onChange, onBlur, value } }) => (
                    <TextInput
                        placeholder="Write Your opinion"
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        numberOfLines={5}
                    />
                )}
                name="review"
            />
            {errors.review && <Text>This is required.</Text>}
            <Text>Read</Text>
            <Controller
                control={control}
                render={({ field: { onChange, value } }) => (
                    <Switch
                        onValueChange={onChange}
                        value={value}
                    />
                )}
                name="finished"
            />
            <Controller
                control={control}
                render={({ field: { onChange, value }}) => (
                    <StarRating
                    rating={value}
                    onChange={onChange}
                />
                )}
                name="stars"
            />
            
            <Button title="Submit" onPress={handleSubmit(onSubmit)} />
            <Button title="Delete" onPress={deleteBook} />

                </ScrollView>
            )
            }
            </View>
            }
            <FlatList horizontal
                data={books}
                renderItem={({ item }) => (
                <Item key={item.id} title={item.title} image={item.image} author={item.author}  id={item.id} />)}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    },
    item:{},
    title:{}
});
