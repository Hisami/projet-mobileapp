import axios from "axios";
import { BookInformation } from "./entities";


 export async function fetchAllBooks() {
    const response = await axios.get<BookInformation[]>(`http://10.0.21.169:8000/api/book_informations`);
    return response.data;
}

export async function fetchOneBook(id:any) {
    const response = await axios.get<BookInformation>('http://10.0.21.169:8000/api/book_informations/'+id);
    return response.data;
}

export async function postBook(bookInformation: BookInformation) {
    console.log(bookInformation)
    const response = await axios.post<BookInformation>('http://10.0.21.169:8000/api/book_informations', bookInformation);
    return response.data;
}

export async function updateBookInformation(bookInformation: BookInformation) {
    const response = await axios.put<BookInformation>('http://10.0.21.169:8000/api/book_informations/'+bookInformation.id, bookInformation);
    return response.data;
}

export async function deleteBookInformation(id:any) {
    await axios.delete('http://10.0.21.169:8000/api/book_informations/'+id);
}





