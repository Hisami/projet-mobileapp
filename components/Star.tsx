import { useState } from 'react';
import StarRating from 'react-native-star-rating-widget';

export const Star = () => {
    const [rating, setRating] = useState(0);
    console.log(rating)
    return (
        <StarRating
            rating={rating}
            onChange={setRating}
        />
    );
};