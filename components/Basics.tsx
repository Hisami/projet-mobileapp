import * as React from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';

// Basic reusable components

export const BookImage = (props:any) => (
  <Image
    style={styles.avatar}
    source={{ uri: props.url }}
  />
);

export const Heading = (props:any) => (
  <Text style={styles.heading}>
    {props.children}
  </Text>
);

export const Title = (props:any) => (
  <Text style={styles.title}>
    {props.children}
  </Text>
);

export const styles = StyleSheet.create({
  avatar: {
    height:112,
    width:80,
  },

  heading: {
    fontSize:20,
    fontWeight:'600',
    paddingTop:20,
    paddingBottom:12,
    paddingHorizontal:24,
    color:"#08050B"
  },
  
  title: {
    color:'#280D5F',
    fontSize:12,
    fontWeight:'600',
    textTransform:'uppercase'
  },
});

