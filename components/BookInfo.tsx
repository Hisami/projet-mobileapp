import { BookImage, Heading, Title } from "./Basics";
import * as React from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { BookInformation } from "../entities";

export const BookInfo = (props: BookInformation) => (
    <View style={bookInfoStyles.card}>
        <BookImage url={props.image} />
        <View style={bookInfoStyles.title}>
            <Title numberOfLines={2}>{props.title}</Title>
            <Text numberOfLines={6}>{props.review}</Text>
        </View>
    </View>
);

export const bookInfoStyles = StyleSheet.create({
    card: {
        width: 300,
        height: 200,
        padding: 12,
        marginRight: 16,
        borderWidth: 1,
        borderColor: '#E7E3EB',
        borderRadius: 12,
        backgroundColor: '#FFFFF',
    },
    title: {
        textAlign: 'center',
        paddingTop: 8
    },
});