import { BookImage, Heading, Title } from "./Basics";
import * as React from 'react';
import { Image, SafeAreaView, ScrollView, StyleSheet, Text, View } from 'react-native';
import { BookInformation } from "../entities";

export const BookCard = (props: BookInformation) => (
    <View style={bookCardStyles.card}>
        <BookImage url={props.image} />
        <View style={bookCardStyles.title}>
            <Title numberOfLines={2}>{props.title}</Title>
        </View>
    </View>
);

export const bookCardStyles = StyleSheet.create({
    card: {
        paddingTop:10,
        width: 150,
        height: 150,
        padding: 12,
        marginRight: 16,
        backgroundColor: '#FFFFF',
    },
    title: {
        textAlign: 'center',
        paddingTop: 8
    },
});